<?php
namespace codehub\tjuploader;

abstract class UploaderService
{
    public $downloadPath = DIRECTORY_SEPARATOR . 'tmp';

    /**
     * Загрузка картинки в хранилище из формы
     * @param  array $file Загруженный с помощью формы файл из $_FILES
     * @return FileInterface
     */
    public abstract function uploadFromFile($file);

    /**
     * Загрузка картинки в хранилище по ссылке
     * @param  string $url Ссылка
     * @return FileInterface
     */
    public abstract function uploadFromUrl($url);


    protected function downloadFileFromUrl($url)
    {
        $path = $this->downloadPath;
        $fileName = uniqid();
        $filePath = $path . DIRECTORY_SEPARATOR . $fileName;
        $result = file_put_contents($filePath, fopen($url, 'r'));
        if ($result === false) {
            throw new \Exception('Remote file is not available');
        }
        return [
            'path' => $filePath,
            'name' => $fileName,
        ];
    }
}