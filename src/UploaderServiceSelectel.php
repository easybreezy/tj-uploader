<?php
namespace codehub\tjuploader;

use ForumHouse\SelectelStorageApi\Authentication\CredentialsAuthentication;
use ForumHouse\SelectelStorageApi\Container\Container;
use ForumHouse\SelectelStorageApi\File\File;
use ForumHouse\SelectelStorageApi\Service\StorageService;

class UploaderServiceSelectel extends UploaderService
{
    /**
     * @var StorageService
     */
    private $service;

    /**
     * @var Container
     */
    private $container;

    public function __construct($authParameters)
    {
        $auth = new CredentialsAuthentication($authParameters['auth_user'], $authParameters['auth_key'],
            $authParameters['auth_url']);

        $auth->authenticate();
        $this->container = new Container($authParameters['container_name']);
        $this->service = new StorageService($auth);
    }

    /**
     * @param array $file
     * @return UploadedFile
     */
    public function uploadFromFile($file)
    {
        $filePath = $file['tmp_name'];
        $fileName = $file['name'];
        return $this->upload($this->getFile($filePath, $fileName));
    }

    /**
     * @param string $url
     * @return UploadedFile
     */
    public function uploadFromUrl($url)
    {
        $file = $this->downloadFileFromUrl($url);
        $filePath = $file['path'];
        $fileName = $file['name'];
        return $this->upload($this->getFile($filePath, $fileName));
    }

    private function upload(File $file)
    {
        $this->service->uploadFile($this->container, $file);
        //кажется fhteam/selectel-storage-api не умеет возвращать ни ссылку на загруженный файл ни его имя
        //хотя в документации selectel есть заголовок ответа X-Uploaded-As
        //так что сделаем вид что библиотека для загрузки в selectel это умеет
        return new UploadedFile('fake');
    }

    private function getFile($filePath, $fileName)
    {
        $file = new File($fileName);
        $file->setLocalName($filePath);
        $file->setSize();
        return $file;
    }
}