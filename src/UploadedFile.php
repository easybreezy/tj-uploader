<?php
/**
 * Created by PhpStorm.
 * User: shock
 * Date: 29.07.2016
 * Time: 3:14
 */

namespace codehub\tjuploader;


class UploadedFile extends FileInterface
{
    /**
     * @var string $url
     */
    private $url;

    public function __construct($url)
    {
        $this->url = $url;
    }

    public function getUrl()
    {
        return $this->url;
    }
}