<?php
namespace codehub\tjuploader;

use Aws\Common\Aws;
use Aws\S3\Model\ClearBucket;
use Aws\S3\S3Client;

class UploaderServiceAmazon extends UploaderService
{
    /**
     * @var S3Client
     */
    private $client;

    /**
     * @var ClearBucket
     */
    private $bucket;

    public function __construct($storageAuthParameters)
    {
        $bucketName = $storageAuthParameters['bucketName'];
        $aws = Aws::factory($storageAuthParameters);
        $this->client = $aws->get('S3');
        $this->bucket = $this->client->createBucket(['Bucket' => $bucketName]);
    }

    /**
     * @param array $file
     * @return UploadedFile
     */
    public function uploadFromFile($file)
    {
        $filePath = $file['tmp_name'];
        $fileName = $file['name'];
        return $this->upload($filePath, $fileName);
    }

    /**
     * @param string $url
     * @return UploadedFile
     */
    public function uploadFromUrl($url)
    {
        $file = $this->downloadFileFromUrl($url);
        $filePath = $file['path'];
        $fileName = $file['name'];
        return $this->upload($filePath, $fileName);
    }

    private function upload($filePath, $fileName)
    {
        $this->client->waitUntil('BucketExists', ['Bucket' => $this->bucket]);
        $model = $this->client->putObject([
            'Bucket'     => $this->bucket,
            'Key'        => $fileName,
            'SourceFile' => $filePath,
        ]);
        return new UploadedFile($model['ObjectURL']);
    }
}