<?php
namespace codehub\tjuploader;

class Uploader implements UploaderInterface
{
    const STORAGE_AMAZON = 1;
    const STORAGE_SELECTEL = 2;

    /**
     * @var UploaderService
     */
    private $service;

    public function __construct($storage, $storageAuthParameters)
    {
        switch ($storage) {
            case self::STORAGE_AMAZON:
                $this->service = new UploaderServiceAmazon($storageAuthParameters);
                break;
            case self::STORAGE_SELECTEL:
                $this->service = new UploaderServiceSelectel($storageAuthParameters);
                break;
            default:
                throw new \Exception('Invalid configuration for ' . Uploader::class . ': unknown storage');
        }
    }

    /**
     * @param array $file
     * @return FileInterface
     */
    public function uploadFromFile($file)
    {
        return $this->service->uploadFromFile($file);
    }

    /**
     * @param string $url
     * @return FileInterface
     */
    public function uploadFromUrl($url)
    {
        return $this->service->uploadFromUrl($url);
    }
}