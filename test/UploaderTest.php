<?php
use codehub\tjuploader\FileInterface;
use PHPUnit\Framework\TestCase;
use codehub\tjuploader\Uploader;

class UploaderTest extends TestCase
{
    private $selectelAuthParams = [
        'auth_url'             => 'https://auth.selcdn.ru',
        'auth_user'            => '20531_test0',
        'auth_key'             => 'RP0crKEwNw',
        'container_name'       => 'test0',
        'container_url'        => 'https://71015.selcdn.ru/test0',
        'container_secret_key' => 'sjdahsasajkslamdakdjh',
    ];

    private $amazonAuthParams = [
        'bucketName'            => 'test_backet',
        //указать свой id
        'aws_access_key_id'     => '',
        //указать свой key
        'aws_secret_access_key' => ''
    ];

    private $fileData = [
        'name'     => 'test.png',
        'type'     => 'image/png',
        'tmp_name' => __DIR__ . '/../images/test.png',
    ];

    private $fileUrl = 'https://cfl.dropboxstatic.com/static/images/icons/blue_dropbox_glyph-vflOJKOUw.png';

    public function testSelectelFromFile()
    {
        $uploader = new Uploader(Uploader::STORAGE_SELECTEL, $this->selectelAuthParams);
        $result = $uploader->uploadFromFile($this->fileData);
        $this->assertInstanceOf(FileInterface::class, $result);
    }

    public function testSelectelFromUrl()
    {
        $uploader = new Uploader(Uploader::STORAGE_SELECTEL, $this->selectelAuthParams);
        $result = $uploader->uploadFromUrl($this->fileUrl);
        $this->assertInstanceOf(FileInterface::class, $result);
    }

    public function testAmazonFromFile()
    {
        $uploader = new Uploader(Uploader::STORAGE_AMAZON, $this->amazonAuthParams);
        $result = $uploader->uploadFromFile($this->fileData);
        $this->assertInstanceOf(FileInterface::class, $result);
    }

    public function testAmazonFromUrl()
    {
        $uploader = new Uploader(Uploader::STORAGE_AMAZON, $this->amazonAuthParams);
        $result = $uploader->uploadFromUrl($this->fileUrl);
        $this->assertInstanceOf(FileInterface::class, $result);
    }
}